import { Component, OnInit } from '@angular/core';
import { ItemsService } from 'src/app/services/items.service';
import { Item } from 'src/app/models/item.model';

@Component({
  selector: 'app-items',
  templateUrl: './items.component.html',
  styleUrls: ['./items.component.css']
})
export class ItemsComponent implements OnInit {
  private getItems: Item[] = [];

  constructor(private itemsService: ItemsService) {
    itemsService.getItems().subscribe(items => this.getItems = items);
  }

  ngOnInit() {
  }

  itemCreated(newItem: Item) {
    this.getItems.push(newItem);
  }

  itemDeleted() {
    this.itemsService.getItems().subscribe(items => this.getItems = items);
  }
}
