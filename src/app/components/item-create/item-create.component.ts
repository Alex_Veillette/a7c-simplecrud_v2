import { Component, OnInit, ViewChild, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators, NgForm } from '@angular/forms';
import { ItemsService } from 'src/app/services/items.service';
import { Item } from 'src/app/models/item.model';

@Component({
  selector: 'app-item-create',
  templateUrl: './item-create.component.html',
  styleUrls: ['./item-create.component.css']
})
export class ItemCreateComponent implements OnInit {
  @Output() readonly itemCreate: EventEmitter<Item> = new EventEmitter();

  @ViewChild('form', {static: true}) formElement: NgForm;

  itemForm: FormGroup;
  nameControl: FormControl;

  constructor(formBuilder: FormBuilder, private itemsService: ItemsService) {
    this.nameControl = new FormControl('', Validators.required);

    this.itemForm = formBuilder.group({ 
        name: this.nameControl,
        description: new FormControl('', [Validators.required])
      } 
    );
  }

  create() {
    if (this.itemForm.valid) {
      const itemValues = this.itemForm.value;
      this.itemsService.create(itemValues.name, itemValues.description).subscribe(newItem => {
        if (newItem) {
          this.itemCreate.emit(newItem);
        }
        else {
          alert('Could not complete create...');
        }
      });
  
      this.formElement.resetForm();
    }
  }

  ngOnInit() {
  }

}
