import { Injectable } from '@angular/core';
import { User } from '../models/user.model';
import { HttpClient } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { map, catchError } from 'rxjs/operators';
import { MarthaService } from './martha.service';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  private currentUser: User;
  
  constructor(private marthaService: MarthaService) {
    this.currentUser = JSON.parse(localStorage.getItem("CU"));
  }

  get getCurrentUser(): User {
    return this.currentUser;
  }

  login(username: string, password: string): Observable<User> {
    return this.marthaService.select('login', { username, password }).pipe(
      map(users => {
        if (users && users.length === 1) {
          const userData = users[0];

          /// On garde le local storage a cause du authguard
          this.currentUser = new User(userData.id, userData.username);
          localStorage.setItem("CU", JSON.stringify(this.currentUser));

          return this.currentUser;
        }
        else {
          return null;
        }
      })
    );
  }

  logout() {
    this.currentUser = null;
    localStorage.setItem("CU", null);
  }

}
