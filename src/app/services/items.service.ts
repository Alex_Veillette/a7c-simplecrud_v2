import { Injectable } from '@angular/core';
import { Item } from '../models/item.model';
import { Observable } from 'rxjs';
import { MarthaService } from './martha.service';
import { AuthService } from './auth.service';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ItemsService {
  private items: Item[];
  private idIncrement: number;

  constructor(private marthaService: MarthaService, private authService: AuthService) {
    this.items = JSON.parse(localStorage.getItem('ITEMS')) || [];
    this.idIncrement = Number(localStorage.getItem('AI')) || 0;
  }

  getItems(): Observable<Item[]> {
    const currentUserId = this.authService.getCurrentUser.id;
    return this.marthaService.select('list-items', {'user_id': currentUserId}).pipe(
      map(items => items || [])
    );
  }

  get(id: number): Item {
    return this.items.find(i => i.id === id);
  }

  create(name: string, description: string): Observable<Item> {
    const user_id = this.authService.getCurrentUser.id;
    return this.marthaService.insert('create-item', {name, description, user_id}).pipe(
      map(id => {
        if (id) {
          return new Item(id, name, description);
        }
        else {
          return null;
        }
      })
    );
  }

  delete(item: Item) {
    return this.marthaService.delete('delete-item', {'item_id': item.id});
  }
}
